#include "hello.h"

#include <iostream>

using namespace std;

int main() {
  cout << "Hello " HELLO_WHO "\n";
  return 0;
}
